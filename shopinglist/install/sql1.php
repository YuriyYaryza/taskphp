<?php

include_once('connectdb.php');
$createProductTable = <<<SQL
    CREATE TABLE IF NOT EXISTS `product` (
      `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `name` varchar(255) DEFAULT NULL,
      `price` double DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SQL;
$res = mysqli_query($db_connect, $createProductTable);